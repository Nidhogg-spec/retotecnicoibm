FROM node:latest

WORKDIR /usr/src/backend

COPY ["package.json", "package-lock.json*", "./" ] 

RUN npm install

COPY . .

EXPOSE 4000

HEALTHCHECK --interval=12s --timeout=12s --start-period=30s \ 
    CMD node healthcheck.js

# RUN npm run deploy
CMD [ "node", "server.js"]