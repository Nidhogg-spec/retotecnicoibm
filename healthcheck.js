const _express = require('express');
const _server = _express();

_server.get('/retoibm/sumar/:sumando01/:sumando02', async function(request, response) {
    try{
        if(typeof request.params !== "undefined" && request.params!==null && !isNaN(request.params)){
            if (response.statusCode == 200) {
                process.exit(0);
              } else {
                process.exit(1);
              }
        } else{
            console.log("Bad request");
            process.exit(1);
        }
    }
    catch(e){
        console.log("ERROR");
        process.exit(1);
    }
});