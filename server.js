const _express = require('express');
const _server = _express();
const {MongoClient} = require('mongodb');

const _port = 4000;
const uri = "mongodb://mongo:27017/docker-node-mongo";

const client = new MongoClient(uri,{ useUnifiedTopology: true });


_server.get('/retoibm/sumar/:sumando01/:sumando02', async function(request, response) {
  await client.connect();
  let coleccion = client.db("RetoTecnicoIBM").collection("Sumas")
  
  try{
    var _sumando01 = new Number(request.params.sumando01);
    var _sumando02 = new Number(request.params.sumando02);
    var _resultado = _sumando01 + _sumando02;
    if (typeof _resultado !== "undefined" && _resultado!==null && !isNaN(_resultado)){ 
      coleccion.insertOne({
        sumando01 : parseInt(_sumando01),
        sumando02 : parseInt(_sumando02),
        resultado : _resultado
      })   
      return response.status(200).json({resultado : _resultado});
    }else{
      return response.status(400).json({resultado : "Bad Request"});
    }
  }
  catch(e){
    return response.status(500).json({resultado : e});
  }
});


_server.listen(_port, () => {
   console.log(`Server listening at ${_port}`);
});